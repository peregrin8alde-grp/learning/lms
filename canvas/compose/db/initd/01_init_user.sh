#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER canvas WITH
      NOSUPERUSER
      NOCREATEDB
      NOCREATEROLE
      PASSWORD 'canvas'
    ;
EOSQL
